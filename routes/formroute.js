var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
  res.send('Get route for handle-submit');
});
router.post('/', function (req, res, next) {
  //Getting form-data
  const info = {
    Contact_name: req.body.name,
    address: req.body.address,
    contact_no: req.body.number,
    emailAddress: req.body.email,
  };
  //Process the data

  //Send respone to client
  res.json({
    info,
    MSG: 'Thank you ! Your form is Submitted with above details',
  });
});

module.exports = router;
