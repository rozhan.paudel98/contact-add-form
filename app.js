var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// Importing Routes
var indexRouter = require('./routes/index');
var formRouter = require('./routes/formroute');

var app = express();
//Declaring Port for app
const PORT = 6500;

// view engine setup if any
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/handle-form', formRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

app.listen(PORT, function (error, done) {
  if (error) {
    console.log('App listen Failed');
  } else {
    console.log('App Listen Success');
  }
});
